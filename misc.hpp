//
//  misc.hpp
//  plink
//
//  Created by Shing Wan Choi on 18/08/2016.
//  Copyright © 2016 Shing Wan Choi. All rights reserved.
//

#ifndef misc_hpp
#define misc_hpp

#include <cmath>
#include <stdexcept>
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <algorithm>
#include <fstream>
#include <limits>
#include <math.h>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace misc
{

// Functions from R
double dnorm(double x, double mu = 0.0, double sigma = 1.0, bool log = false);
double qnorm(double p, double mu = 0.0, double sigma = 1.0,
             bool lower_tail = true, bool log_p = false);


inline std::ifstream load_stream(const std::string& filepath)
{
    auto file = std::ifstream(filepath.c_str());
    if (!file.is_open())
    { throw std::runtime_error("Error: Cannot open file: " + filepath); }
    return file;
}
inline std::vector<std::string> split(const std::string& seq,
                                      const std::string& separators = "\t ")
{
    std::size_t prev = 0, pos;
    std::vector<std::string> result;
    while ((pos = seq.find_first_of(separators, prev)) != std::string::npos)
    {
        if (pos > prev) result.emplace_back(seq.substr(prev, pos - prev));
        prev = pos + 1;
    }
    if (prev < seq.length()) result.emplace_back(seq.substr(prev, pos - prev));
    return result;
}
template <typename T>
inline T convert(const std::string& str)
{
    std::istringstream iss(str);
    T obj;

    iss >> std::ws >> obj >> std::ws;

    if (!iss.eof()) throw std::runtime_error("Unable to convert the input");

    return obj;
}
// trim functions from https://stackoverflow.com/a/217605
// trim from start (in place)
inline std::string_view ltrimmed(std::string_view s)
{
    s.remove_prefix(std::distance(
        s.cbegin(), std::find_if(s.cbegin(), s.cend(),
                                 [](int ch) { return std::isgraph(ch); })));

    return s;
}

inline std::string_view rtrimmed(std::string_view s)
{
    s.remove_suffix(std::distance(
        s.crbegin(), std::find_if(s.crbegin(), s.crend(),
                                  [](int ch) { return std::isgraph(ch); })));
    return s;
}

inline std::string_view trimmed(std::string_view s)
{
    return ltrimmed(rtrimmed(s));
}
inline void ltrim(std::string_view& s)
{
    s.remove_prefix(std::distance(
        s.cbegin(), std::find_if(s.cbegin(), s.cend(),
                                 [](int ch) { return std::isgraph(ch); })));
}

inline void rtrim(std::string_view& s)
{
    s.remove_suffix(std::distance(
        s.crbegin(), std::find_if(s.crbegin(), s.crend(),
                                  [](int ch) { return std::isgraph(ch); })));
}

inline void trim(std::string_view& s)
{
    rtrim(s);
    ltrim(s);
}


inline void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return std::isgraph(ch); }));
};

// trim from end (in place)
inline void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return std::isgraph(ch); })
                .base(),
            s.end());
};
// trim from both ends (in place)
inline void trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
};
// trim from start (copying)
inline std::string ltrimmed(std::string s)
{
    ltrim(s);
    return s;
};
// trim from end (copying)
inline std::string rtrimmed(std::string s)
{
    rtrim(s);
    return s;
};
// trim from both ends (copying)
inline std::string trimmed(std::string s)
{
    trim(s);
    return s;
};

/*!
 * \brief Function to check if two double are equal from
 *        https://stackoverflow.com/a/4010279/1441789
 * \param a the first double
 * \param b the second double
 * \param error_factor level of error, should be of no concern to us at the
 *        moment
 * \return True if two double are equal
 */
inline bool logically_equal(double a, double b, double error_factor = 1.0)
{
    return ((a == b)
            || (std::abs(a - b) < std::abs(std::min(a, b))
                                      * std::numeric_limits<double>::epsilon()
                                      * error_factor));
}
}
#endif /* misc_hpp */
